public class Board {

    private Square[][] tictactoeBoard;

    public Board(){
        
        tictactoeBoard = new Square[][]{
            {Square.BLANK, Square.BLANK, Square.BLANK},
            {Square.BLANK, Square.BLANK, Square.BLANK},
            {Square.BLANK, Square.BLANK, Square.BLANK}
        };
    }
    
    @Override
    public String toString()
    {   
        String result = "";
        for (int i =0; i < this.tictactoeBoard.length; i++){
            for (int j=0; j< this.tictactoeBoard[i].length;j++){
                result += (this.tictactoeBoard[i][j]+ " ");
            }
            result += ('\n');
        }
        return result;
    }   


    public boolean placeToken (int row,int col, Square playerToken){
        
        if (row == 3 && col == 3){
            return true;
        }

        else if (tictactoeBoard[row][col] == Square.BLANK){
            tictactoeBoard[row][col] = playerToken;
            return true;
        }
        
        else{
            return false;
        }
    }

    public boolean checkIfFull(){ 
        boolean checking = true;

        for(int i=0; i < tictactoeBoard.length ; i++){
            for(int j=0;j < tictactoeBoard[i].length; j++){
                if (tictactoeBoard[i][j] == Square.BLANK){
                    checking = false;
                }
            }
        }
        return checking;
    }
    
    private boolean checkIfWinningHorizontal (Square playerToken){
        boolean horizontal = false;
        
        if(tictactoeBoard[0][0] == playerToken && tictactoeBoard[0][1] == playerToken && tictactoeBoard[0][2] == playerToken){
            horizontal = true;
        }

        else if(tictactoeBoard[1][0] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[1][2] == playerToken){
            horizontal = true;
        }

        else if (tictactoeBoard[2][0] == playerToken && tictactoeBoard[2][1] == playerToken && tictactoeBoard[2][2] == playerToken){
            horizontal = true;
        }

        else{
            horizontal = false;
        }

        return horizontal;
    }

    private boolean checkIfWinningVertical (Square playerToken){
        boolean vertical = false;

        if(tictactoeBoard[0][0] == playerToken && tictactoeBoard[1][0] == playerToken && tictactoeBoard[2][0] == playerToken){
            vertical = true;
        }

        else if(tictactoeBoard[0][1] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[2][1] == playerToken){
            vertical = true;
        }

        else if(tictactoeBoard[0][2] == playerToken && tictactoeBoard[1][2] == playerToken && tictactoeBoard[2][2] == playerToken){
            vertical = true;
        }

        else{
            vertical = false;
        }

        return vertical;

    }

    public boolean checkIfWinning(Square playerToken){
        if(checkIfWinningHorizontal(playerToken) == true || checkIfWinningVertical(playerToken) == true){
            return true;
        }

        else{
            return false;
        }
    }
}
