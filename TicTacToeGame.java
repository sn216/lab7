import java.util.Scanner;
public class TicTacToeGame {
    public static void main (String [] args){
  
        Board game = new Board();
        Scanner scan = new Scanner(System.in);

        System.out.println("Hello There, are you ready to play a game?");

        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;

        while(gameOver == false){
            System.out.println(game.toString());

            if(player == 1){
                playerToken = Square.X;
            }

            else{
                playerToken = Square.O;
            }

            int numRow = scan.nextInt();
            int numCol = scan.nextInt();

            boolean token = game.placeToken(numRow, numCol, playerToken);

            while(token == false){
                numRow = scan.nextInt();
                numCol = scan.nextInt();
                
                if(numRow < 3 && numCol < 3){
                    token = true;
                }
            }
            

            if(game.checkIfFull() == true){
                System.out.println("It's a tie!");
                gameOver = true;
            }
            
            else if(game.checkIfWinning(playerToken)){
                System.out.println(playerToken + " has won!");
                gameOver = true;
            }

            else{
                player = player + 1;

                if(player > 2){
                    player = 1;
                }
            }


        }
    }
}
